package repo

import ps "temp/genproto/user"

type UserStorageI interface{
	CreateUser(*ps.User) (*ps.User, error)
	GetUser(userID int64) (*ps.User, error)
	UpdateUser(*ps.User) (*ps.User, error)
	DeleteUser(userID int64) (*ps.User, error)
	
}