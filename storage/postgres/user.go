package postgres

import (
	"fmt"
	ps "temp/genproto/user"

	"github.com/jmoiron/sqlx"
)

type userRepo struct{
	db	*sqlx.DB
}
func NewUserRepo (db *sqlx.DB) *userRepo{
	return &userRepo{
		db: db,
	}
}

func (u *userRepo) CreateUser(req *ps.User) (*ps.User, error) {
	user := ps.User{}
	err := u.db.QueryRow(`insert into users(first_name, last_name) values($1, $2) 
	returning id, first_name, last_name`,req.FirstName, req.LastName).Scan(&user.Id, &user.FirstName, &user.LastName)
	if err != nil{
		return &ps.User{}, err
	}
	return &user, nil
}
func (u *userRepo) GetUser(userID int64) (*ps.User, error) {
	user := ps.User{}
	err := u.db.QueryRow(`select id, first_name, last_name from users where id = $1`,userID).Scan(&user.Id, &user.FirstName, &user.LastName)
	if err != nil{
		return &ps.User{}, err
	}
	return &user, nil
}
func (u *userRepo) UpdateUser(req *ps.User) (*ps.User, error) {
	user := ps.User{}
	_,err := u.db.Exec(`update users SET first_name = $1, last_name = $2 where id = $3`,req.FirstName, req.LastName, req.Id)
	if err != nil{
		return &ps.User{}, err
	}
	return &user, nil
}
func (u *userRepo) DeleteUser(userID int64) (*ps.User, error) {
	user := ps.User{}
	_,err := u.db.Query(`delete from users where id = $1`,userID)
	if err != nil{
		return &ps.User{}, err
	}
	return &user, nil
}

func (u *userRepo) CheckField(req *ps.CheckFieldReq) (*ps.CheckFieldResp, error) {
	query := fmt.Sprintf(`select 1 from users where`)
}
