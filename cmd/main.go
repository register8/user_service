package main

import (
	"net"
	"temp/config"
	ps "temp/genproto/user"
	"temp/pkg/db"
	"temp/pkg/logger"
	"temp/service"
	"temp/service/client"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()


	log := logger.New(cfg.LogLevel, "user-service")
	defer logger.Cleanup(log)
	
	log.Info("main: sqlxConfig",
logger.String("host",cfg.RPCPort),
logger.Int("port",cfg.PostgresPort),
logger.String("database",cfg.PostgresDatabase))

connDB, err := db.ConnectToDB(cfg)
if err != nil{
	log.Fatal("sqlx connection to postgres error",logger.Error(err))
}
grpcClient, err := client.New(cfg)
if err != nil{
	log.Fatal("sqlx connection to postgres error",logger.Error(err))
}

userService := service.NewUserService(connDB, log, grpcClient)

lis, err := net.Listen("tcp", cfg.RPCPort)
if err != nil{
	log.Fatal("Error while listening: %v", logger.Error(err))
}

s := grpc.NewServer()
reflection.Register(s)
ps.RegisterUserServiceServer(s, userService)
log.Info("main: server running",
logger.String("port",cfg.RPCPort))

if err := s.Serve(lis); err != nil{
	log.Fatal("Error while listening: %v", logger.Error(err))
}

}