package client

import (
	"fmt"
	"temp/config"
	pbp "temp/genproto/product"

	"google.golang.org/grpc"
)

type GrpcClientI interface{
	Product() pbp.ProductServiceClient
}

type GrpcClient struct{
	cfg config.Config
	productService pbp.ProductServiceClient
}

func New(cfg config.Config) (*GrpcClient, error){
	connProduct, err := grpc.Dial(
		fmt.Sprintf("%s:%d",cfg.ProductServiceHost, cfg.ProductServicePort),
		grpc.WithInsecure())
	if err != nil{
		return nil, fmt.Errorf("post service dial host: %s port: %d",
	cfg.ProductServiceHost,cfg.ProductServicePort)
	}
	return &GrpcClient{
		cfg: cfg,
		productService: pbp.NewProductServiceClient(connProduct),
	}, nil
}

func (p *GrpcClient) Product() pbp.ProductServiceClient{
	return p.productService
}