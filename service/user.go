package service

import (
	"context"
	ps "temp/genproto/user"
	"temp/pkg/logger"
	"temp/service/client"
	"temp/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct{
	storage storage.IStorage
	logger logger.Logger
	client client.GrpcClientI
}

func NewUserService(db *sqlx.DB, log logger.Logger, client client.GrpcClientI) *UserService{
	return &UserService{
	storage: storage.NewStoragePg(db),
	logger: log,
	client: client,
	}
}

func (u *UserService) CreateUser(ctx context.Context, req *ps.User) (*ps.User, error) {
	user, err := u.storage.User().CreateUser(req)
	if err != nil{
		u.logger.Error("Error with UserR", logger.Any("error INSERT UserR", err))
		return &ps.User{}, status.Error(codes.Internal, "something went wrong, please check product info")
	}
	return user, nil
}
func (u *UserService) GetUser(ctx context.Context, req *ps.UserID) (*ps.User, error) {
	user, err := u.storage.User().GetUser(req.UserId)
	if err != nil{
		u.logger.Error("Error with UserR", logger.Any("error GETTING UserR", err))
		return &ps.User{}, status.Error(codes.Internal, "something went wrong, please check product info")
	}
	return user, nil
}
func (u *UserService) UpdateUser(ctx context.Context, req *ps.User) (*ps.User, error) {
	user, err := u.storage.User().UpdateUser(req)
	if err != nil{
		u.logger.Error("Error with UserR", logger.Any("error UPDATE UserR", err))
		return &ps.User{}, status.Error(codes.Internal, "something went wrong, please check product info")
	}
	return user, nil
}
func (u *UserService) DeleteUser(ctx context.Context, req *ps.UserID) (*ps.User, error) {
	user, err := u.storage.User().DeleteUser(req.UserId)
	if err != nil{
		u.logger.Error("Error with UserR", logger.Any("error Delete UserR", err))
		return &ps.User{}, status.Error(codes.Internal, "something went wrong, please check product info")
	}
	return user, nil
}